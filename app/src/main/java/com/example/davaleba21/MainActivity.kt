package com.example.davaleba21

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.davaleba21.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding : ActivityMainBinding
    private val item  = mutableListOf<Menu>()
    private lateinit var adapter : DrawerRecyclerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getMenuItems()
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.nav_home), binding.drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        adapter.checkReturn()
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }



    private fun getMenuItems(){
        val navController = findNavController(R.id.nav_host_fragment)

        item.add(Menu(R.drawable.ic_menu_camera,"Camera"))
        item.add(Menu(R.drawable.ic_menu_gallery,"Galerry"))
        item.add(Menu(R.drawable.ic_menu_slideshow,"SlideShow"))

        adapter = DrawerRecyclerAdapter(item)
        binding.drawerMenuRecycler.layoutManager = LinearLayoutManager(this)
        adapter.selectedItem ={

            binding.drawerLayout.closeDrawer(GravityCompat.START)
            when(it){
                0 -> navController.navigate(R.id.nav_home)

                1 -> navController.navigate(R.id.nav_gallery)

                2 -> navController.navigate(R.id.nav_slideshow)

            }
        }
        binding.drawerMenuRecycler.adapter = adapter
    }
    
}