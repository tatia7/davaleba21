package com.example.davaleba21


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.davaleba21.databinding.RecyclerMenuBinding

typealias selectedItem = (position : Int) -> Unit

class DrawerRecyclerAdapter(private val item : MutableList<Menu>): RecyclerView.Adapter<DrawerRecyclerAdapter.ViewHolder>() {


    lateinit var selectedItem :  selectedItem

    var selected : Int = 0

    inner class ViewHolder(val binding: RecyclerMenuBinding) :
        RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        private lateinit var model : Menu

        override fun onClick(v: View?) {
            selectedItem.invoke(adapterPosition)
            selected=adapterPosition
            notifyDataSetChanged()
        }

        fun bind() {

            model = item[adapterPosition]
            binding.icon.setImageResource(model.image)
            binding.title.text = model.title

            binding.root.setOnClickListener(this)

            if (adapterPosition == selected){
                binding.root.setBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.purple_200))
                binding.view.setBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.purple_200))
            }else{
                binding.view.visibility = View.INVISIBLE
                binding.root.setBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.teal_200))

            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            RecyclerMenuBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = item.size


    fun checkReturn(){
        selected = 0
        notifyDataSetChanged()
    }

}

